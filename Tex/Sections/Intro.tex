\chapter{Introduction} % Main chapter title

\label{Chapter1} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

\lhead{Chapter 1. \emph{Introduction}} % Change X to a consecutive number; this is for the header on each page - perhaps a shortened title

Determination of what exactly happens when a piece of Malware is executed is often a time consuming process - time which many organizations do not have, whether it is due to the need for highly available systems, ongoing infection, or due to the sheer complexity of the sample.

Malware can range from being relatively simple, such as downloading further rogue software, to much more complex samples, where parts of the sample are encrypted, consist of multiple components, or are highly obfuscated. Dependent on the information required, analysis can take long periods of time - in the case of the Flame malware, Alexander Gostev (Kaspersky) has said: \textit{“It will take us 10 years to fully understand everything”}.

As Security researchers, we do not have many years to analyse malware, and as such tools must be used to decrease the workload in analysis, and decrease the time taken. One such tool available is a \textit{sandbox} - a tool which runs a program in a secure environment, mitigating the risk of infection to the host machine.

\section{Cuckoo Sandbox}
Cuckoo Sandbox is a set of free open source sandbox software, developed by the Cuckoo foundation to assist in the automation of analysis of suspicious files. This is achieved through the use of a set of custom components which monitor sample processes running in an isolated environment. It is used to automatically run and analyse files and from this, collect comprehensive results which show what the sample does when executed in an isolated windows system. It is capable of finding the following results:
\begin{itemize}
	\item Traces of win32 API calls performed by all processes spawned by the malware.
	\item Files being created, deleted and downloaded by the malware during its execution.
	\item Memory dumps of the malware processes.
	\item Network traffic trace in PCAP format.
	\item Screenshots of Windows desktop taken during the execution of the malware.
	\item Full memory dumps of the machines.
\end{itemize}
\clearpage
Cuckoo is designed to be used both standalone and integrated into a larger malware analysis framework, and can be used to analyse:
\begin{itemize}
	\item Generic Windows executables
	\item DLL files
	\item PDF documents
	\item Microsoft Office documents
	\item URLs and HTML files
	\item PHP scripts
	\item CPL files
	\item Visual Basic (VB) scripts
	\item ZIP files
	\item Java JAR
	\item Python files
	\item \textit{Almost anything else}
\end{itemize}

Cuckoo sandbox consists of a central management system (handling sample execution and analysis) and a series of virtual machines. Each analysis performed is launched in a fresh virtual machine, isolated from the wider local network. Although Cuckoo was designed to run on GNU/Linux, with Windows XP SP3 as Guest, we have also had success in running it on Mac OS X.

\subsection{What is Sandboxing?}
Malware sandboxing is the application of dynamic analysis - rather than deconstructing and analysing how a sample is constructed, the sample is executed and monitored. Anything the sample does upon execution, including changes to the filesystem, and network activity is logged.

Cuckoo Sandbox works by injecting and executing malware inside a virtualised environment; this virtual environment is useful because it provides a safe place for execution (i.e. not on a system connected to a local network, nor having sensitive information) and allows the researcher to leverage snapshots (Explained further in Section 1.1.2). Multiple virtual environments can be run a single system, which reduces the hardware required.

This method has its drawbacks however, as virtualisation systems leave traces which can make it easy to detect if an operating system is being virtualised. As such it is best to conduct a static analysis of the sample as well to gain a deep understanding of it.


\subsection{What is Virtualisation?}
Virtualisation is the process of creating a virtual version of something, such as an operating system, server, or storage/network device. Hardware virtualisation is the process of creating a virtual machine (VM) which acts like a real computer with its own operating system - running at a separation from the underlying hardware.

This is unlike a standard computer, whereby the operating system runs directly on hardware, with no supervisory layer between. When discussing hardware virtualisation, two terms are often used: the \textit{host}, and the \textit{guest}. The \textit{host} refers to the physical machine on which virtualisation occurs, whilst the \textit{guest} is the virtual machine running. The software used to create and run virtual machines on a \textit{host} is called a \textit{hypervisor}.
Two types of hypervisor exist:
	\begin{itemize}
		\item Type 1: Bare Metal
			\begin{itemize}
				\item A bare metal hypervisor runs directly on the host hardware, and is used to control both the hardware and guest machines. Guest machines then run as processes on the hypervisor, and generally have greater access to the underlying hardware, as there is not much overhead from the hypervisor. An example of a Type 1 Hypervisor would b Microsoft's Hyper-V server 2008/12 or VMware ESXi.
			\end{itemize}
		\item Type 2: Hosted
			\begin{itemize}
				\item A hosted hypervisor runs as an application on a conventional operating system, and abstracts the guest system from the host system. An example of these hypervisors would be VMware Workstation/Player, or Virtualbox.
			\end{itemize}
	\end{itemize}

Virtualisation can allow an organisation to reduce their costs by deploying several operating systems onto a single piece of hardware, providing savings both in terms of power and hardware acquisition. However, the advantage to using virtualised systems for malware analysis is being able to utilise snapshots. As the full operating system is virtualised, a snapshot of the exact state a system is in can be saved and rolled back to as needed. This means that if a system becomes infected or ceases to function correctly, it can be reverted to a known working state rather than having to reinstall from scratch - rolling back to a snapshot can take a very short amount of time dependent on the speed of drive used.

Additionally, many hypervisors enable  \textit{guest machines} to use a concept known as \textit{dynamic memory}; this allows a maximum and minimum memory value to be specified for each virtual machine - rather than assign the maxmimum to each machine, memory is assigned as needed to each virtual machine. If one VM requires more memory than another, and that second machine is not actively using it, the hypervisor can reassign the memory to the VM requiring it. Using this method, less memory than would normally be required can be used in a system. 

Once a system has been deployed, an Administrator can clone it, and use the clone instantly, without having to reinstall/reconfigure the full system. We can use this to speed up analysis, running scans asynchronously.

\section{Tutorial Notes}
\begin{itemize}
	\item This tutorial assumes a prior knowledge of the Linux terminal and Bash. 
	\item Sections marked aside by a horizontal line are command input to the terminal.
\end{itemize}

