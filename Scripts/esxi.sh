#!/bin/bash

# TODO:
# * Explain VM Set Up

prerequisites () {
	# Install the prerequisites for building before we begin installation of required packages
	# Update repositories & upgrade all packages
	echo "Now installing prerequisites"
	sudo apt-get -y update
	sudo apt-get -y upgrade
	# Install the python headers and package manager, as well as the tools required to compile later
	sudo apt-get -y install python-pip build-essential python-dev linux-headers-$(uname -r)
};

stage1a (){
	# Separate stage for installing Yara
	# Install prerequisites - automake, and libtool
	echo "Now installing Yara prerequisites"
	sudo apt-get -y install libtool automake
	# Clone jansson repository, required to install yara
	cd ~/Downloads
	git clone https://github.com/akheron/jansson
	cd jansson
	# Make configure executable and run
	chmod +x configure
	./configure
	# Install Jansson
	make
	sudo make install
	cd ~/Downloads
	# Download  and unpack Yara Source
	echo "Installing Yara"
	wget https://github.com/plusvic/yara/archive/v3.2.0.tar.gz
	tar -xzvf v3.2.0.tar.gz
	cd yara-3.2.0
	# Configure the build environment to include cuckoo,
	# Then make and install
	./bootstrap.sh
	./configure --enable-cuckoo --enable-magic
	make
	sudo make install
	# Finally enter and build the python library
	cd yara-python
	python setup.py build
	sudo setup.py install
	cd ~/Downloads
};

stage1b (){
	# Separate stage for installing SSDeep
	# Download and unpack SSDeep source
	echo "Installing SSDeep"
	wget http://downloads.sourceforge.net/project/ssdeep/ssdeep-2.12/ssdeep-2.12.tar.gz
	tar -xzvf ssdeep-2.12.tar.gz
	cd ssdeep-2.12
	# Configure, Make, Check the make and install
	./configure
	make
	make check
	sudo make install
	cd ~/Downloads
};

stage1c (){
	# Separate stage for installing PyDeep
	# Download & unpack pydeep source
	echo "Installing PyDeep"
	wget https://github.com/kbandla/pydeep/archive/master.zip
	unzip master.zip
	cd pydeep-master
	# Build and install the library using the python installer
	python setup.py build
	sudo python setup.py install
	export LD_LIBRARY_PATH=/usr/local/lib
	cd ~/Downloads
};

stage1 () {
	# Install packages as required from pip - 
	# these are the prerequisite packages required to run cuckoo sandbox, and optional extras.
	# Yara, SSDeep and PyDeep are installed through the following functions:
	# stage1a (Yara), stage1b (SSDeep), stage1c (PyDeep)
	#
	# For portability it is easier to define and install these packages from pip.
	echo "Installing Prerequisites"
	sudo pip install --upgrade sqlalchemy bson jinja2 pymongo bottle pefile maec==4.0 django chardet python-magic mako
	# These packages are not available through pip
	sudo apt-get -y install libmagic python-dpkt
	# Commencing Yara, SSDeep, PyDeep
	echo "Installing Yara, SSDeep, PyDeep"
	stage1a
	stage1b
	stage1c
};

stage2 (){
	# Compiling the libvirt library from source
	# The original library does not ship with support for esxi - in order to use with esxi we must recompile with the correct flags.
	# Unfortunately this is a long and tedious process.
	#
	# Begin by installing the required prerequisite packages. 
	# You may notice some overlap between this step and the first, 
	# this is due to it breaking when run on one set of systems, working on others.
	echo "Installing Prerequisites"
	sudo apt-get -y install gcc make pkg-config libxml2-dev libgnutls-dev libdevmapper-dev libcurl4-gnutls-dev libpciaccess-dev libnl-dev libyajl-dev
	# Grab the source for libvirt, and unpack
	echo "Installing libvirt"
	wget ftp://libvirt.org/libvirt/libvirt-1.2.9.tar.gz
	tar -xzvf libvirt-1.2.9.tar.gz
	cd libvirt-1.2.9
	# Configure, make and install the new library
	./configure --with-esx=yes
	make
	sudo make install
	# Finally, install the python-libvirt library
	echo "Installing Python-libvirt"
	sudo apt-get install -y python-libvirt
	cd ~/Downloads
};

stage3 (){
	# Sorting out the tcpdump
	# This allows us to capture all network traffic between the virtual machines!
	# Install tcpdump and associated library
	echo "Configuring tcpdump"
	sudo apt-get -y install tcpdump libcap2-bin
	sudo setcap cap_net_raw,cap_net_admin=eip /usr/sbin/tcpdump
};

stage4 (){
	# Installing Cuckoo
	#
	# For those of you still with us, we are downloading the source and extracting it.
	# This should be the final stage of automation, Some manual work will have to be done now.
	#
	# Download cuckoo source, and unpack
	echo "Installing Cuckoo"
	wget http://downloads.cuckoosandbox.org/cuckoo-current.tar.gz
	tar -xzvf cuckoo-current.tar.gz
};

fullInstall (){
	# Ask for sudo password up front
	sudo -v
	prerequisites
	stage1
	stage2
	stage3
	stage4
};

opt1 (){
	sudo -v
	stage1
};
opt2 (){
	sudo -v
	stage2
};
opt3 (){
	sudo -v
	stage3
};
opt4 (){
	sudo -v
	stage4
};



title="Cuckoo Sandbox Automated Installation Script"
prompt="Pick an option:"
options=("Full Installation" "Stage 1 Install" "Stage 2 Install" "Stage 3 Install" "Stage 4 Install")

echo "$title"
PS3="$prompt "
select opt in "${options[@]}" "Quit"; do 

    case "$REPLY" in

    1 ) fullInstall;;
    2 ) stage1;;
    3 ) stage2;;
	4 ) stage3;;
	5 ) stage4;;

    $(( ${#options[@]}+1 )) ) echo "Goodbye!"; break;;
    *) echo "Invalid option. Try another one.";continue;;

    esac

done




